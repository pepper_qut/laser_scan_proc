/*
 * Created on Tue Apr 24 2018
 *
 * The MIT License (MIT)
 * Copyright (c) 2018 Gavin Suddrey - Queensland University of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
#include <math.h>

using namespace sensor_msgs;

ros::Publisher pub;

float angle_min = std::numeric_limits<float>::infinity();
float angle_max = std::numeric_limits<float>::infinity();

void callback(const LaserScanConstPtr& scan) {
  LaserScan sliced = *scan;
  sliced.angle_min = angle_min;
  sliced.angle_max = angle_max;

  int start_index = (int)ceil((angle_min - scan->angle_min) / scan->angle_increment);
  int bins = (int)ceil(floor(fabs(angle_min) + fabs(angle_max)) / scan->angle_increment);

  std::vector<float> ranges;
  ranges.resize(bins, -1);

  ranges.insert(ranges.begin(), 
                scan->ranges.begin() + start_index, 
                scan->ranges.begin() + start_index + bins);

  sliced.ranges = ranges;

  pub.publish(sliced);
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "laser_slice");

  ros::NodeHandle nh;
  ros::NodeHandle _nh("~");

  if (!_nh.getParam("angle_min", angle_min)) {
    ROS_ERROR("No angle_min was set");
    return -1;
  }

  if (!_nh.getParam("angle_max", angle_max)) {
    ROS_ERROR("No angle_max was set");
    return -1;
  }

  pub = nh.advertise<LaserScan>("scan/sliced", 50);
  ros::Subscriber sub = nh.subscribe("scan", 1, callback);

  ros::spin();

  return 0;
}
