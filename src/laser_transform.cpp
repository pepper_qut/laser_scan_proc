/*
 * Created on Tue Apr 24 2018
 *
 * The MIT License (MIT)
 * Copyright (c) 2018 Gavin Suddrey - Queensland University of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
#include <tf/transform_listener.h>

using namespace sensor_msgs;

class LaserTransform {
public:
  LaserTransform()
    : _nh(), _nh_("~"), _frame_id("/base_link") {

    if (!_nh_.getParam("frame_id", _frame_id)) {
      ROS_INFO("No frame_id was set - using /base_link");
    }

    std::cout << "Frame: " << _frame_id << std::endl;
    
    _pub = _nh.advertise<LaserScan>("scan/transformed", 50);
    _sub = _nh.subscribe("scan", 1, &LaserTransform::callback, this);
  }

  void callback(const LaserScanConstPtr& scan) {
    tf::StampedTransform transform;

    LaserScan transformed = *scan;

    try {
      _listener.waitForTransform(scan->header.frame_id, _frame_id, 
                                ros::Time(0), ros::Duration(5.0));
      _listener.lookupTransform(scan->header.frame_id, _frame_id,
                               ros::Time(0), transform);

      double roll, pitch, yaw;
      transform.getBasis().getRPY(roll, pitch, yaw);

      transformed.header.frame_id = _frame_id;
      transformed.angle_min -= yaw;
      transformed.angle_max -= yaw;

      _pub.publish(transformed);

    } catch (tf::TransformException ex) {
      ROS_ERROR("%s",ex.what());
    }
  }

private:
  ros::NodeHandle _nh;
  ros::NodeHandle _nh_;

  std::string _frame_id;

  ros::Publisher _pub;
  ros::Subscriber _sub;

  tf::TransformListener _listener;
};

int main(int argc, char** argv) {
  ros::init(argc, argv, "laser_filter");

  LaserTransform transformer;

  ros::spin();

  return 0;
}
